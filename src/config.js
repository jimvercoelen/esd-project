export const BASE_URL = __PROD__ ? 'still unknown' : 'http://localhost'
export const API_URL = __PROD__ ? BASE_URL : BASE_URL + ':8080'
