import React from 'react'
import styles from './Header.scss'
import Content from 'common/components/Content'

const Header = () => (
  <div className={styles.component}>
    <Content className={styles.content}>
      <div className={styles.branding}>
        ESD
      </div>
    </Content>
  </div>
)

export default Header
