import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Router } from 'react-router'
import createRoutes from 'modules/router'

export default class Root extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired
  }

  render () {
    const extraProps = {
      key: __DEV__ ? Math.random() : undefined
    }

    return (
      <Router
        {...extraProps}
        history={this.props.history}
        routes={createRoutes()} />
    )
  }
}
