import React from 'react'
import styles from './Footer.scss'
import Content from 'common/components/Content'

const Footer = () => (
  <div className={styles.component}>
    <Content className={styles.content}>
      <div className={styles.copywrite}>
        © ESD-project 2017. All Rights Reserved.
      </div>
    </Content>
  </div>
)

export default Footer
