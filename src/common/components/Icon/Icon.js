import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import styles from './Icon.scss'

const Icon = ({ glyph, className, ...props }) => (
  <svg
    className={cx({
      [styles.component]: true,
      [className]: className
    })}
    {...props}>
    <use xlinkHref={glyph} />
  </svg>
)

Icon.propTypes = {
  glyph: PropTypes.string.isRequired,
  className: PropTypes.string
}

export default Icon
