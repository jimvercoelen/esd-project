import React from 'react'
import PropTypes from 'prop-types'
import styles from './AppLayout.scss'

import Header from 'common/components/Header'
import Footer from 'common/components/Footer'


const AppLayout = ({ children }) => (
  <div className={styles.component}>
    <Header />
    {children}
    <Footer />
  </div>
)

AppLayout.propTypes = {
  children: PropTypes.node.isRequired
}

export default AppLayout
