import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { browserHistory } from 'react-router'
import Root from 'common/components/Root'

export const rootElement = document.getElementById('root')

if (__DEV__) {
  import('react-hot-loader').then(({ AppContainer }) => {
    // Trick babel to avoid hoisting <AppContainer />
    // transform-react-constant-elements
    const noHoist = {}

    render((
      <AppContainer {...noHoist}>
        <Root history={browserHistory} />
      </AppContainer>
    ), rootElement)

    if (module.hot) {
      module.hot.accept('./common/components/Root', () => {
        import('./common/components/Root').then((nextRoot) => {
          const NextRootContainer = nextRoot.default

          render((
            <AppContainer>
              <NextRootContainer history={browserHistory} />
            </AppContainer>
          ), rootElement)
        })
      })
    }
  })
} else {
  render((
    <Root history={browserHistory} />
  ), rootElement)
}
