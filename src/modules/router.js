import AppLayout from 'common/components/Layouts/AppLayout'

import Home from './Home'
import NotFound from './NotFound'

import * as Routes from 'constants/Routes'

export default () => {
  return {
    component: AppLayout,
    childRoutes: [
      {
        path: Routes.HOME,
        indexRoute: Home(),
        childRoutes: [
          // Empty
        ]
      },
      NotFound()
    ]
  }
}
