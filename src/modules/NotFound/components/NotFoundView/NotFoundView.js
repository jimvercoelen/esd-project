import React from 'react'
import styles from './NotFoundView.scss'
import GraveIcon from './Grave.svg'
import Icon from 'common/components/Icon'

const NotFoundView = () => (
  <div className={styles.component}>
    <div className={styles.content}>
      <Icon glyph={GraveIcon} className={styles.icon} />
      <h1 className='leading'>
        Woops!
      </h1>
      <h2 className='subleading'>
        We couldn't find the page you were looking for.
      </h2>
    </div>
  </div>
)

export default NotFoundView
