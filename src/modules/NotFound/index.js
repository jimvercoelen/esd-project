export default (store) => ({
  path: '*',
  getComponent: async function (nextState, cb) {
    const module = await import('./components/NotFoundView')
    cb(null, module.default)
  }
})
