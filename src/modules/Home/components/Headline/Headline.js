import React from 'react'
import styles from './Headline.scss'
import Content from 'common/components/Content'
import cx from 'classnames'

const Headline = () => (
  <div className={styles.component}>
    <Content className={styles.content}>
      <h1 className={cx('leading', styles.title)}>
        ESD - Project
      </h1>
      <h2 className={cx('subleading', styles.subtitle)}>
        Oh Captain, my captain
      </h2>
    </Content>
  </div>
)

export default Headline
