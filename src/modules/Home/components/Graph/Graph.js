import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import _ from 'lodash'
import styles from './Graph.scss'

import Content from 'common/components/Content'
import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from 'recharts'

export default class Graphs extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired
  }

  static defaultProps = {
    // Empty
  }

  render () {
    const formattedData = _.map(this.props.data, d => {
      return {
        ...d,
        date: moment(d.date).format("hh:mm:ss a")
      }
    }).reverse()

    if (formattedData.length === 0) {
      return (
        <div>
          Loading ...
        </div>
      )
    }

    return (
      <div className={styles.component}>
        <Content className={styles.content}>
          <ResponsiveContainer>
            <LineChart data={formattedData}>
              <XAxis dataKey="date" />
              <YAxis />
              <CartesianGrid strokeDasharray="3 3" />
              <Tooltip />
              <Legend />
              <Line type="monotone" dataKey="tmp" stroke="#82ca9d" />
            </LineChart>
          </ResponsiveContainer>
        </Content>
      </div>
    )
  }
}
