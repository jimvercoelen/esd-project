import React from 'react'
import Headline from '../Headline'
import Graph from '../Graph'

const HomeView = (props) => (
  <div>
    <Headline {...props} />
    <Graph {...props} />
  </div>
)

export default HomeView
