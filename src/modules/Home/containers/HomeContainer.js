import React, { Component } from 'react'
import HomeView from '../components/HomeView'
import 'whatwg-fetch'

const initialState = {
  intervalId: null,
  data: []
}

export default class HomeContainer extends Component {

  componentDidMount () {
    const intervalId = setInterval(() => {
      fetch('http://jim-express-api.azurewebsites.net/data')
        .then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json()
          }

          const error = new Error(response.statusText)
          error.response = response
          throw error
        })
        .then((json) => this.setState({ data: json }))
        .catch((error) => console.log('parsing failed', error))
    }, 5000)

    this.setState({ intervalId })
  }

  componentWillUnmount () {
    clearInterval(this.state.intervalId)
  }

  constructor (props) {
    super(props)

    this.state = initialState
  }

  render () {
    return (
      <HomeView data={this.state.data} />
    )
  }
}
